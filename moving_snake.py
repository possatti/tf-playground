import tensorflow as tf
import numpy as np
import sys

def print_tensor(*tensors, message=None):
	if message is None:
		message = '{}' * len(tensors)
	def print_and_return(*xs):
		print(message.format(*xs))
		return xs
	dtypes = [ t.dtype for t in tensors ]
	shapes = [ t.shape for t in tensors ]
	new_tensors = tf.py_func(print_and_return, tensors, dtypes)
	for new_tensor, shape in zip(new_tensors, shapes):
		new_tensor.set_shape(shape)
	return new_tensors

size = tf.Variable(3)

coords = tf.Variable(np.array([
	[2,2],
	[3,2],
	[4,2],
]))

direction = tf.constant([0,1], tf.int64)

step = tf.Variable(0)

board_shape = tf.constant([10,10], dtype=tf.int64)

def walk(coords, size, direction):
	cut_tail = coords[:size-1]
	old_head_coord = coords[0]
	new_head_coord = old_head_coord + direction
	new_coords = tf.concat([[new_head_coord], cut_tail], axis=0)
	# print("size:", size, file=sys.stderr) #!#
	# new_coords.set_shape([size,2])
	# print("new_coords:", new_coords, file=sys.stderr) #!#
	return new_coords

def loop_body(coords, step):
	new_coords = walk(coords, size, direction)
	new_step = step + 1

	one_values = tf.ones(shape=[size], dtype=tf.int64)
	print("new_coords:", new_coords, file=sys.stderr) #!#
	print("one_values:", one_values, file=sys.stderr) #!#
	print("board_shape:", board_shape, file=sys.stderr) #!#
	board = tf.scatter_nd(new_coords, one_values, board_shape)
	print("board:", board, file=sys.stderr) #!#

	# print_step = tf.Print(new_step, [new_step], message='step: ', summarize=1)
	# print_coords, = print_tensor(new_coords, message='coords:\n{}')
	print_board, = print_tensor(board, message='board:\n{}')
	print("print_board:", print_board, file=sys.stderr) #!#

	return tf.tuple([new_coords, new_step], control_inputs=[print_board])
	# return print_coords, print_step

final_coords, final_step = tf.while_loop(lambda coords, step: step < 4, loop_body, [coords, step], shape_invariants=[tf.TensorShape([None,2]), tf.TensorShape([])])

with tf.Session() as sess:
	sess.run(tf.global_variables_initializer())
	print('initial_coords:\n', sess.run(coords), sep='')
	print('final_step:\n', sess.run(final_step), sep='')

