import tensorflow as tf
import numpy as np
import sys

def print_and_return(x):
	print(x)
	return x

x = tf.constant([1,2,3,4])
print("x:", x, file=sys.stderr) #!#
x = tf.py_func(print_and_return, [x], x.dtype)
print("x:", x, file=sys.stderr) #!#

with tf.Session() as sess:
	sess.run(tf.global_variables_initializer())
	print('result x:\n', sess.run(x), sep='')
