import tensorflow as tf
import numpy as np
import sys

def expand_coords(coords, width, height):
	shape = (height, width)
	board = tf.scatter_nd(coords, tf.ones(coords.shape[0], dtype=tf.int16), shape)
	return board

coords = tf.Variable(np.array([
	[0,0],
	[0,1],
	[2,2],
	[3,3],
	[3,4],
	[3,5],
	[6,6],
]))

board = expand_coords(coords, 7, 7)

with tf.Session() as sess:
	sess.run(tf.global_variables_initializer())
	print('coords:\n', sess.run(coords), sep='')
	print('board:\n', sess.run(board), sep='')
