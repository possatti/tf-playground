#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from sklearn.metrics import confusion_matrix

import tensorflow as tf
import pandas as pd
import numpy as np
import argparse
import math
import sys
import os

LEBAL_NAME_TO_DENSE = {
	'Iris-setosa': 0,
	'Iris-versicolor': 1,
	'Iris-virginica': 2,
}

def from_csv(iris_path):
	dataset = tf.contrib.data.make_csv_dataset(args.iris_data, batch_size=32)

	estimator = tf.estimator.DNNClassifier(
		feature_columns=[sparse_feature_a_emb, sparse_feature_b_emb],
		hidden_units=[1024, 512, 256],
		optimizer=tf.train.ProximalAdagradOptimizer(
			learning_rate=0.1,
			l1_regularization_strength=0.001
		))

def create_dataset_from_csv(iris_data_path):
	reader = tf.TextLineReader()
	key, record_strings = reader.read(iris_data_path)
	print("key:", key, file=sys.stderr) #!#
	print("record_strings:", record_strings, file=sys.stderr) #!#
	raw_features, label_name = tf.decode_csv(record_strings, [tf.float32,tf.float32,tf.float32,tf.float32,tf.string])

	# features_train, labels_train, features_test, labels_test = tf.train_split(features, labels, frac=.1)
	# return features_train, labels_train, features_test, labels_test

def create_dataset_using_pandas(iris_data_path, shuffle_buffer_size=None):
	N_CLASSES = 3

	df = pd.read_csv(iris_data_path, names=['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'label_name'])
	df['label'] = [ LEBAL_NAME_TO_DENSE[name] for name in df['label_name'] ]

	features = np.array(df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']])
	labels = np.zeros(shape=(len(df), N_CLASSES))
	labels[range(len(df)),df['label']] = 1

	if shuffle_buffer_size == None:
		shuffle_buffer_size = len(df)

	dataset = tf.data.Dataset.from_tensor_slices((features,labels))
	dataset = dataset.shuffle(shuffle_buffer_size)
	return dataset

def train_dnn(dataset, epochs=10, batch_size=32):
	dataset = dataset.batch(batch_size).repeat()
	n_classes = dataset.output_shapes[1][1]

	x, y = dataset.make_one_shot_iterator().get_next()

	with tf.variable_scope('Network'):
		net = tf.layers.dense(x, 10, activation=tf.nn.relu)
		net = tf.layers.dense(net, 8, activation=tf.nn.relu)
		net = tf.layers.dense(net, 6, activation=tf.nn.relu)
		logits = tf.layers.dense(net, n_classes, activation=None)

	loss = tf.losses.softmax_cross_entropy(y, logits)
	# loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=logits)
	optimizer = tf.train.AdamOptimizer(learning_rate=0.04)
	train_op = optimizer.minimize(loss, global_step=tf.train.create_global_step())

	y_argmax = tf.argmax(y, axis=1)
	logits_argmax = tf.argmax(logits, axis=1)

	with tf.variable_scope('Metrics'):
		acc, acc_op = tf.metrics.accuracy(labels=y_argmax, predictions=logits_argmax)
		batch_acc = tf.reduce_mean(tf.cast(tf.equal(y_argmax, logits_argmax), tf.float32))
		# batch_acc = tf.reduce_mean(tf.cast(y_argmax == logits_argmax, tf.float32))
		# batch_acc = tf.reduce_mean(tf.to_float32(y_argmax == logits_argmax))

	acc_vars = tf.contrib.framework.get_variables('Metrics/accuracy', collection=tf.GraphKeys.LOCAL_VARIABLES)
	acc_reset_op = tf.variables_initializer(acc_vars)

	tf.summary.scalar('loss', loss)
	tf.summary.scalar('acc', batch_acc)
	summaries_op = tf.summary.merge_all()

	with tf.Session() as sess:
		sess.run(tf.local_variables_initializer())
		sess.run(tf.global_variables_initializer())

		train_writer = tf.summary.FileWriter('./logs/iris', sess.graph)

		for i in range(epochs):
			sess.run(acc_reset_op)
			epochs_steps_n = math.ceil(len(df)/BATCH_SIZE)
			for j in range(epochs_steps_n):
				fetches = {
					'train_op': train_op,
					'loss': loss,
					'acc': acc_op,
					'summaries': summaries_op,
					'global_step': tf.train.get_global_step(),
				}
				results = sess.run(fetches)
				print("Epoch: {}, Iter: {}, Loss: {:.4f}, Acc: {:.4f}".format(i, j, results['loss'], results['acc']))
				train_writer.add_summary(results['summaries'], results['global_step'])

# def silly(iris_path):
# 	EPOCHS = 10
# 	BATCH_SIZE = 50
# 	N_CLASSES = 3
# 	SHUFFLE_BUFFER_SIZE = 150

# 	df = pd.read_csv(iris_path, names=['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'label_name'])
# 	df['label'] = [ LEBAL_NAME_TO_DENSE[name] for name in df['label_name'] ]
# 	print("df.head():", df.head(), file=sys.stderr) #!#
# 	features = np.array(df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']])
# 	labels = np.zeros(shape=(len(df), N_CLASSES))
# 	labels[range(len(df)),df['label']] = 1

# 	print("features.shape:", features.shape, file=sys.stderr) #!#
# 	print("labels.shape:", labels.shape, file=sys.stderr) #!#

# 	dataset = tf.data.Dataset.from_tensor_slices((features,labels)).shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE).repeat()
# 	iter_ = dataset.make_one_shot_iterator()
# 	x, y = iter_.get_next()

# 	with tf.variable_scope('Network'):
# 		net = tf.layers.dense(x, 10, activation=tf.nn.relu)
# 		net = tf.layers.dense(net, 8, activation=tf.nn.relu)
# 		net = tf.layers.dense(net, 6, activation=tf.nn.relu)
# 		logits = tf.layers.dense(net, N_CLASSES, activation=None)

# 	loss = tf.losses.softmax_cross_entropy(y, logits)
# 	# loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=logits)
# 	optimizer = tf.train.AdamOptimizer(learning_rate=0.04)
# 	train_op = optimizer.minimize(loss, global_step=tf.train.create_global_step())

# 	y_argmax = tf.argmax(y, axis=1)
# 	logits_argmax = tf.argmax(logits, axis=1)

# 	with tf.variable_scope('Metrics'):
# 		acc, acc_op = tf.metrics.accuracy(labels=y_argmax, predictions=logits_argmax)
# 		# batch_acc = tf.reduce_mean(tf.cast(tf.equal(y_argmax, logits_argmax), tf.float32))
# 		batch_acc = tf.reduce_mean(tf.to_float32(y_argmax == logits_argmax))

# 	acc_vars = tf.contrib.framework.get_variables('Metrics/accuracy', collection=tf.GraphKeys.LOCAL_VARIABLES)
# 	acc_reset_op = tf.variables_initializer(acc_vars)

# 	tf.summary.scalar('loss', loss)
# 	tf.summary.scalar('acc', batch_acc)
# 	summaries_op = tf.summary.merge_all()

# 	with tf.Session() as sess:
# 		sess.run(tf.local_variables_initializer())
# 		sess.run(tf.global_variables_initializer())

# 		train_writer = tf.summary.FileWriter('./logs/iris', sess.graph)

# 		for i in range(EPOCHS):
# 			sess.run(acc_reset_op)
# 			epochs_steps_n = math.ceil(len(df)/BATCH_SIZE)
# 			for j in range(epochs_steps_n):
# 				fetches = {
# 					'train_op': train_op,
# 					'loss': loss,
# 					'acc': acc_op,
# 					'summaries': summaries_op,
# 					'global_step': tf.train.get_global_step(),
# 				}
# 				results = sess.run(fetches)
# 				print("Epoch: {}, Iter: {}, Loss: {:.4f}, Acc: {:.4f}".format(i, j, results['loss'], results['acc']))
# 				train_writer.add_summary(results['summaries'], results['global_step'])

def main():
	# Arguments and options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--iris-data', default='/dados/datasets/iris/iris.data')
	args = parser.parse_args()

	# silly(args.iris_data)
	dataset = create_dataset_using_pandas(args.iris_data)
	train_dnn(dataset)

if __name__ == '__main__':
	main()