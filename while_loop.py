# https://www.dotnetperls.com/while-tensorflow

import tensorflow as tf

# Pass vars as a tuple.
# ... The number of items in the tuple must match.
#     These are the start values.
#     First lambda: keep repeating body while this is true.
#     Second lambda: get values on each iteration.
a, b = tf.while_loop(lambda a, b: a < 30,
    lambda a, b: (a * 3, b * 2),
    (2, 3))

# Run the while loop and get the resulting values.
with tf.Session() as sess:
	result = sess.run([a, b])
	print(result)
